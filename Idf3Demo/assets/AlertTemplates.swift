//
//  alertTemplates.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/11/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit

class AlertTemplates{
    
    static let alert = UIAlertController(title: nil, message: Constants.wait, preferredStyle: .alert)
    
    
    
    static func showAlert(vc:UIViewController,title:String,msg:String,clickTxt:String)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: clickTxt, style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showAlert(vc:UIViewController,title:String,msg:String)  {
        showAlert(vc: vc, title: title, msg: msg, clickTxt: Constants.ok)
    }
    
    static func showAlert(vc:UIViewController,msg:String)  {
        showAlert(vc: vc, title: Constants.error, msg: msg, clickTxt: Constants.ok)
    }
    
    static func showDialog(vc:UIViewController) {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func dismissDialog() {
        alert.dismiss(animated: true, completion: nil)
    }
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
