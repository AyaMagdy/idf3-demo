//
//  checkInternet.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/18/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
