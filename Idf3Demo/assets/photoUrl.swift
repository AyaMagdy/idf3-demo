//
//  photoUrl.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/12/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit

class photoUrl {
    
    static func getImage(image:UIImageView,url:String)  {
        image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placehoulder.png"))
    }
}
 
//extension String {
//
//    func changeStringToArabic () -> String{
//        return "alf"
//    }
//
//}
