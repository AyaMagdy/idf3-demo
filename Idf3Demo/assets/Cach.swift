//
//  Cash.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/11/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
class Cach {
    let jsonDecoder = JSONDecoder()

    static func saveUser(user:User){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey: Constants.USER)
    }
    
    static func saveUser(passcode:Int){
        UserDefaults.standard.set(passcode, forKey: Constants.PASSCODE)
    }

    static func saveUser(user:User,passcode:Int){
        UserDefaults.standard.set(passcode, forKey: Constants.PASSCODE)
        saveUser(user:user)
    }


    static func getPasscode() -> Int {
        return UserDefaults.standard.integer(forKey: Constants.PASSCODE)
    }

    static func getUser() -> User? {
        if let data = UserDefaults.standard.value(forKey:Constants.USER) as? Data {
            let user = try? PropertyListDecoder().decode(User.self, from: data)
            return user
            
        }
        return nil
    }
}
