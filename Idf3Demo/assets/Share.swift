//
//  Share.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/16/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit

class Share {
    
    // share text
    static func shareText(vc:UIViewController,text:String) {
        
        // set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        vc.present(activityViewController, animated: true, completion: nil)
        
    }
    
    // share image
    static func shareImage(vc:UIViewController,image:UIImage) {
        
        
        // set up activity view controller
        let imageToShare = [image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc.view // so that
        
        // present the view controller
        vc.present(activityViewController, animated: true, completion: nil)
    }
}
