//
//  DateFormat.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
class DateFormat {
    static var dateFormatterFrom : DateFormatter?
    static var dateFormatterTo : DateFormatter?
    
static func convertDate(date:String)-> String{
    if dateFormatterFrom == nil || dateFormatterTo == nil{
        dateFormatterFrom = DateFormatter()
        dateFormatterFrom?.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterFrom?.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        dateFormatterTo = DateFormatter()
        dateFormatterTo?.locale = Locale.current
        dateFormatterTo?.dateFormat = "dd/MM/yyyy"
    }
    
    
    if let date = dateFormatterFrom?.date(from: date) {
        return dateFormatterTo?.string(from: date) ?? ""
    } else {
        return ""
    }
}
}
