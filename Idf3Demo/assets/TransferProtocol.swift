//
//  TransferProtocol.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/14/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

protocol TransferProtocol {
    
    func completeTransaction(phone:String)
}
