//
//  CustomButtons.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit

class CustomButtons
{
    static let customBtnObj = CustomButtons()
    func backBtn(NC : UINavigationController ,NI :UINavigationItem ){
        let imgBack = UIImage(named: "back-arrow")
        NC.navigationBar.backIndicatorImage = imgBack
        NC.navigationBar.backIndicatorTransitionMaskImage = imgBack
        NI.leftItemsSupplementBackButton = true
        NC.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
}
