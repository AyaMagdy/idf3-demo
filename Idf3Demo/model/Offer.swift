//
//  Offer.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
struct Offer : Codable {
    var name : String = ""
    var description : String = ""
    var discount: String = ""
    var photo: String = ""
}
