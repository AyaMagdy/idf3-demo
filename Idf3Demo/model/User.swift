//
//  User.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct User : Codable {
    var id = 0
    var name = ""
    var phone = ""
    var credit_amount = ""
    var verification_code = ""
    var photo = ""
    var user_token = ""
    var fcm_token = ""
    var created_at = ""
    
    private enum CodingKeys : String, CodingKey {
        case    id = "id",
                name = "name",
                phone = "phone" ,
                credit_amount = "credit_amount",
                verification_code = "verification_code",
                photo = "photo",
                user_token = "user_token",
                fcm_token = "fcm_token",
                created_at = "created_at"
        
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? ""
        credit_amount = try container.decodeIfPresent(String.self, forKey: .credit_amount) ?? ""
        verification_code = try container.decodeIfPresent(String.self, forKey: .verification_code) ?? ""
        photo = try container.decodeIfPresent(String.self, forKey: .photo) ?? ""
        user_token = try container.decodeIfPresent(String.self, forKey: .user_token) ?? ""
        fcm_token = try container.decodeIfPresent(String.self, forKey: .fcm_token) ?? ""
        created_at = try container.decodeIfPresent(String.self, forKey: .created_at) ?? ""
    }
}
