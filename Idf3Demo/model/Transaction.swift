//
//  Transaction.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Transaction : Codable {
    var name : String = ""
    var phone : String = ""
    var is_sender: Bool = false
    var credit_amount: String = ""
    var created_at: String = ""
}
