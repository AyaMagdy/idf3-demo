//
//  UIImageView.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/18/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setRounded() {
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}
