//
//  UIButton.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/18/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setButtonRounded(){
    self.layer.cornerRadius = 10
    self.clipsToBounds = true
    }
}
