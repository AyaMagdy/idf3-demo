//
//  ApiResult.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
enum ApiResult<Value> {
    case success(Value)
    case failure(String)
}
