//
//  ApiFunctions.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import Alamofire

class ApiRequests {
    
    static let ApiObject = ApiRequests()
    private let BASE_URL = "https://ntam.tech/E-Wallet/api"
    
    
    func login(phone : String , fcm_token : String ,completion: @escaping (ApiResult<User>) -> Void)  {
        
        let url = "\(BASE_URL)/user_register"
        let params = ["phone" : phone ,"fcm_token" : fcm_token]
        
        Alamofire.request(url , method : .post , parameters: params).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponse<User>.self, from: jsonData!)
                    if dataResponse.status {
                        let user = dataResponse.data
                        completion(.success(user))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case .failure(let error):
                // default by swift
                print("from api : \(error)")
                if !Connectivity.isConnectedToInternet() {
                     completion(.failure("لا يوجد انترنت"))
                     return
                }
                completion(.failure(error.localizedDescription))
            }
        }
        
    }
    
    func getUserObject(userToken:String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/balance"
        let params = ["user_token": userToken]
        
        Alamofire.request(url , method : .post , parameters: params).responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(ApiResponse<User>.self, from: jsonData!)
                        if dataResponse.status {
                            let user = dataResponse.data
                            completion(.success(user))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getUserUsingPhone(phone: String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/get_data_using_phone"
        let params = ["phone": phone]
       Alamofire.request(url , method : .post , parameters: params).responseJSON { (response) in
        print("from api response is : \(response)")
                switch response.result {
                case .success:
                    let jsonData = response.data
                    print("json data : \(String(describing: jsonData))")
                    do{
                        let dataResponse = try JSONDecoder().decode(ApiResponse<User>.self, from: jsonData!)
                        print("after parse \(dataResponse)")
                        if dataResponse.status {
                            let user = dataResponse.data
                            completion(.success(user))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func updateUser(name: String,photo:String,userToken:String,completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/update_profile"
        let params = ["name": name,"photo":photo,"user_token": userToken]
        
        Alamofire.request(url, method: .post, parameters: params)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(ApiResponse<User>.self, from: jsonData!)
                        if dataResponse.status {
                            let user = dataResponse.data
                            completion(.success(user))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
            }
    }
    
    
    func transferMoney(userToken: String,toPhone:String,amount:String,completion: @escaping (ApiResult<String>) -> Void) {
        print("from apiiiiiiii\(toPhone)")
        let url = "\(BASE_URL)/transfer_credit"
        let params = ["user_token": userToken,"receiver_phone":"\(toPhone)","credit_amount":amount]
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponseString.self, from: jsonData!)
//                    print(dataResponse)
                    if dataResponse.status {
                        completion(.success(dataResponse.message))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case let .failure(error):
                completion(.failure(error.localizedDescription))
            }
        }
    }
    
    func history(user_token: String,completion: @escaping (ApiResult<[Transaction]>) -> Void) {
        let url = "\(BASE_URL)/history"
        let params = ["user_token": user_token]
        
        Alamofire.request(url , method : .post , parameters: params).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponseDictionary<Transaction>.self, from: jsonData!)
                    if dataResponse.status {
                        let transactions = dataResponse.data
                        completion(.success(transactions))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case let .failure(error):
                completion(.failure(error.localizedDescription))
            }
        }
    }

    
    
    func offers(completion: @escaping (ApiResult<[Offer]>) -> Void) {
        let url = "\(BASE_URL)/offers"
        
        Alamofire.request(url , method : .get).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponseDictionary<Offer>.self, from: jsonData!)
                    if dataResponse.status {
                        let offers = dataResponse.data
                        completion(.success(offers))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case let .failure(error):
                completion(.failure(error.localizedDescription))
            }
        }
    }
    
    
    func about(completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/about"
        
        Alamofire.request(url , method : .get).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponseString.self, from: jsonData!)
                    if dataResponse.status {
//                        let about = dataResponse.data
                        completion(.success(dataResponse.data))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case let .failure(error):
                completion(.failure(error.localizedDescription))
            }
        }
    }
    
    func recharge(userToken: String,code:String,completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/get_credit"
        let params = ["user_token": userToken,"barcode":code]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonData = response.data
                do{
                    let dataResponse = try JSONDecoder().decode(ApiResponseString.self, from: jsonData!)
                    if dataResponse.status {
                        completion(.success(dataResponse.message))
                    } else {
                        completion(.failure(dataResponse.message))
                    }
                }catch {
                    completion(.failure(error.localizedDescription))
                }
            case let .failure(error):
                completion(.failure(error.localizedDescription))
            }
        }
        
        
    }
    
    
}
