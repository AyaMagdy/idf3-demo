//
//  BaseResponse.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct ApiResponse <ResponseType: Decodable>: Decodable  {
    let status : Bool
    let message : String
    let data : ResponseType
}


struct ApiResponseDictionary <ResponseType: Decodable>: Decodable  {
    let status : Bool
    let message : String
    let data : [ResponseType]
}


struct ApiResponseString: Decodable  {
    let status : Bool
    let message : String
    let data : String!
}
