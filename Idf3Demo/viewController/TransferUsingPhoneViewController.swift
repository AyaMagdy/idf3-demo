//
//  TransferUsingPhoneViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class TransferUsingPhoneViewController: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!
    @IBOutlet weak var resultView: UIView!
    
    var transferProtocol : TransferProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // to custom back button
        CustomButtons.customBtnObj.backBtn(NC: self.navigationController!, NI: self.navigationItem)
        
        
    }
        
    @IBAction func phoneEntered(_ sender: Any) {
        if let phoneNumber = phoneNumber.text , phoneNumber.count == 11 {
            // request user info
            getReciverInfo(phoneNumber)
        }
    }
    
    @IBAction func sendBtn(_ sender: Any) {
        //getReciverInfo(phoneNumber.text!)
        if self.userImage.isHidden == false {
            self.transferProtocol?.completeTransaction(phone: phoneNumber.text ?? "")
        }
        else
        {
            AlertTemplates.showAlert(vc: self, msg:"لا يمكنك التحويل لرقم غير مسجل")
        }
    }
    
    func getReciverInfo(_ phoneNumber : String){
       AlertTemplates.showDialog(vc: self)
        ApiRequests.ApiObject.getUserUsingPhone(phone:phoneNumber)
        { result in
            print("result is : \(result)")
            switch result {
            case let .success(user):
                print(user)
                AlertTemplates.dismissDialog()
                self.viewBorder()
                self.userName.text = user.name
                let url = URL(string: user.photo)
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    let image = UIImage(data: imageData)
                    self.userImage.image = image
                    self.userImage.isHidden = false
                }
                self.userName.isHidden = false
                self.userName.text = user.name
                self.inviteBtn.isHidden = true
                break
            case .failure(_):
                  AlertTemplates.dismissDialog()
                  self.viewBorder()
                  self.userImage.isHidden = true
                  self.userName.textAlignment = .right
                  self.userName.text = "هذا الرقم غير مسجل "
                  self.userName.isHidden = false
                  self.inviteBtn.isHidden = false
                break
            }
        }
    }
    
    @IBAction func sendInvitation(_ sender: Any) {
        if let user = Cach.getUser() {
            let str = "\(String(describing: user.phone)) \(Constants.shareTxtInfo) \(Constants.appLink)"
            Share.shareText(vc: self, text: str)
        }
    }
    func viewBorder()
    {
//        self.resultView.layer.masksToBounds = true
        self.resultView.layer.borderWidth = 0.3
    }
}
