//
//  EditProfileViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/12/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Photos
class EditProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var photo: UIImageView!
    var photoBase64 = ""

    var imagePicker = UIImagePickerController()
    
    override func viewWillLayoutSubviews() {
        photo.setRounded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
//        checkPermission()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.selectImage))
        self.photo.isUserInteractionEnabled = true
        self.photo.addGestureRecognizer(gesture)
        
        setProfileData()
    }
    
    func setProfileData(){
        if let user = Cach.getUser(){
            name.text = user.name
            photoUrl.getImage(image:photo,url:user.photo)
        }
    }
    

    @IBAction func saveChange(_ sender: Any) {
        if name.text?.isEmpty ?? true {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidName)
        }else {
            if let image = photo.image {
             let imageData = image.jpegData(compressionQuality: 0.75)
             photoBase64 = imageData!.base64EncodedString(options: .lineLength64Characters)
            }
            updateProfile()
        }
    }
    
    func updateProfile() {
        if let userName = name.text {
            if let currentUser = Cach.getUser() {
                AlertTemplates.showDialog(vc: self)
                ApiRequests.ApiObject.updateUser(name: userName, photo: photoBase64,userToken: currentUser.user_token)
                { result in
                    switch result {
                    case let .success(user):
                        Cach.saveUser(user: user)
                        AlertTemplates.dismissDialog()
                        AlertTemplates.showAlert(vc: self,title:Constants.success, msg: Constants.profileUpdated)
                        break
                    case let .failure(error):
                        AlertTemplates.dismissDialog()
                        AlertTemplates.showAlert(vc: self, msg:error)
                        break
                    }
                }
            }
        }
    }
    
    @objc func selectImage() {
        let alert = UIAlertController(title: "تغيير الصورة", message: "اختر المصدر", preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: "الكاميرا", style: .default) { (result : UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "مكتبة الصور", style: .default) { (result : UIAlertAction) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "الغاء", style: .cancel) { (result : UIAlertAction) -> Void in
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
                if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    print("d5al el image picker")
                 photo.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
//    func checkPermission() {
//        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
//        switch photoAuthorizationStatus {
//        case .authorized: print("Access is granted by user")
//        case .notDetermined:
//            PHPhotoLibrary.requestAuthorization({
//            (newStatus)
//                in print("status is \(newStatus)")
//                if newStatus == PHAuthorizationStatus.authorized {  print("success") }
//            })
//            case .restricted:
//                print("User do not have access to photo album.")
//            case .denied:
//                print("User has denied the permission.")
//            }
//        }
}
