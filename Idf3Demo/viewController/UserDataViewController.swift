//
//  UserDataViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/11/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserDataViewController: UIViewController {
    var user:User?
    

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var pinCodeTextField: UITextField!
    @IBOutlet weak var rePinCodeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pinCodeTextField.keyboardType = .asciiCapableNumberPad
        rePinCodeTextField.keyboardType = .asciiCapableNumberPad
        
        getUserName()
    }
    
    func getUserName()  {
        if let currentUser = user {
            nameTextField.text = currentUser.name
        }
    }
    
    @IBAction func finishBtn(_ sender: Any) {
        checkData()
    }
    
    func checkData(){
        if nameTextField.text?.isEmpty ?? true {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidName)
        }
        else if pinCodeTextField.text?.count != 4 {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidPasscode)
        }else if pinCodeTextField.text != rePinCodeTextField.text {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidConfirmPasscode)
        }else {
            updateUserName()
        }
    }
    
    
    func updateUserName()
    {
        if let userName = nameTextField.text {
            if let currentUser = user {
                AlertTemplates.showDialog(vc: self)
                ApiRequests.ApiObject.updateUser(name: userName, photo: "",userToken: currentUser.user_token ) { result in
                    switch result {
                    case let .success(user):
                        self.user = user
                        
                        let passcode = Int(self.pinCodeTextField.text ?? "0") ?? 0
                        
                        Cach.saveUser(user: user, passcode:passcode)
                        AlertTemplates.dismissDialog()
                        let VC = self.storyboard!.instantiateViewController(withIdentifier: "pinCode") as! PinCodeViewController
                        self.present(VC, animated:true, completion: nil)
                        break
                    case let .failure(error):
                        AlertTemplates.dismissDialog()
                        AlertTemplates.showAlert(vc: self, msg:error)
                        break
                    }
                }
            }
        }
    }
}
