//
//  ProfileViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/12/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import AVFoundation

class ProfileViewController: UIViewController {

    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var qrCodeImage: UIImageView!
    
    override func viewWillLayoutSubviews() {
        photo.setRounded()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomButtons.customBtnObj.backBtn(NC: self.navigationController!, NI: self.navigationItem)
        firstView.layer.borderColor = UIColor.gray.cgColor
        secondView.layer.borderColor = UIColor.gray.cgColor
        firstView.layer.cornerRadius = 10
        secondView.layer.cornerRadius = 10

         setUserData()
    }
    func setUserData(){
        if let user = Cach.getUser(){
            name.text = user.name
            phone.text = user.phone
            photoUrl.getImage(image:photo,url:user.photo)
            generateQRCode(phone: user.phone)
        }
    }
    
    
    func generateQRCode(phone:String){
        var qrcodeImage: CIImage!
        let data = phone.data(using: .ascii, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        qrcodeImage = (filter?.outputImage)!
        let scaleX = qrCodeImage.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = qrCodeImage.frame.size.height / qrcodeImage.extent.size.height
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        let qrCode = convertCIImageToUiImage(transformedImage: transformedImage)
        qrCodeImage.image = qrCode
    }
    
    func convertCIImageToUiImage(transformedImage:CIImage?) -> UIImage? {
        if let ciImage = transformedImage {
            let context:CIContext = CIContext.init(options: nil)
            let cgImage:CGImage = context.createCGImage(ciImage, from: ciImage.extent)!
            return UIImage.init(cgImage: cgImage)
        }
        return nil
    }
    @IBAction func sharePhone(_ sender: Any) {
        if let phone = phone.text {
            let str = "\(phone) \(Constants.shareTxtInfo) \(Constants.appLink)"
            Share.shareText(vc: self, text: str)
        }
    }
    @IBAction func shareQRcode(_ sender: Any) {
        if let qrimage = qrCodeImage {
            Share.shareImage(vc: self, image:qrimage.image!)
        }
    }
  
}
