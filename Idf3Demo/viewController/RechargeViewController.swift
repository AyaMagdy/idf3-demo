//
//  RechargeViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/16/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class RechargeViewController: UIViewController , TransferProtocol {


    @IBOutlet weak var scanQrView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.scanQR))
        self.scanQrView.isUserInteractionEnabled = true
        self.scanQrView.addGestureRecognizer(gesture)
        
        
        self.scanQrView.layer.cornerRadius = self.scanQrView.frame.size.width/2
        self.scanQrView.clipsToBounds = true
        self.scanQrView.layer.masksToBounds = true
//        self.scanQrView.layer.borderColor = .opaqueBlack
//        self.scanQrView.layer.backgroundColor = UIColor.lightGray.cgColor
        self.scanQrView.layer.borderWidth = 1.0
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ScanQRCodeViewController {
            let vc = segue.destination as? ScanQRCodeViewController
            vc?.transferProtocol = self
        }
    }
    
    @objc func scanQR() {
        let newViewController = ScanQRCodeViewController()
        newViewController.transferProtocol = self
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func completeTransaction(phone: String) {
        recharge(code: phone)
    }
    
    
    func recharge(code:String) {
        if let user = Cach.getUser() {
            AlertTemplates.showDialog(vc: self)
            ApiRequests.ApiObject.recharge(userToken: user.user_token, code: code)  { result in
                switch result {
                case .success(_):
                    AlertTemplates.dismissDialog()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                        AlertTemplates.showAlert(vc: self, title:"نجحت العملية",msg: "تم شحن الرصيد")
                    }
                    break
                case let .failure(error):
                    AlertTemplates.dismissDialog()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                       AlertTemplates.showAlert(vc: self, msg:error)
                    }
                    break
                }
            }
        }else {
            AlertTemplates.showAlert(vc: self, title:"فشلت العملية",msg: "حدث خطا ما")
        }
    }
    
}
