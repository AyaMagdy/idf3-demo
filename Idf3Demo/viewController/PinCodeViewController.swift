//
//  PinCodeViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/11/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class PinCodeViewController: UIViewController {

    @IBOutlet weak var pinCodeTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        pinCodeTextField.keyboardType = .asciiCapableNumberPad

    }
    

    @IBAction func finishBtn(_ sender: Any) {
        checkPinCode()
    }
    func checkPinCode(){
        if pinCodeTextField.text?.count == 4 {
            let passcodeCach = Cach.getPasscode()
            
             let passcode = Int(self.pinCodeTextField.text ?? "0") ?? 0
            
            if passcodeCach == passcode {
                performSegue(withIdentifier: "homePage", sender: self)
            }else {
                AlertTemplates.showAlert(vc:self , msg:Constants.invalidPasscodeLogin)
            }
        }
        else{
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidVerificationCount)
        }
    }
    
}
