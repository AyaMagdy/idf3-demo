//
//  AboutViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomButtons.customBtnObj.backBtn(NC: self.navigationController!, NI: self.navigationItem)
         getAbout()
    }
    

    func getAbout()
    {
        AlertTemplates.showDialog(vc: self)
        ApiRequests.ApiObject.about()
            { result in
                switch result {
                case let .success(about):
                    AlertTemplates.dismissDialog()
                    self.webview.loadHTMLString("\(about)", baseURL: nil)

                    break
                case let .failure(error):
                    AlertTemplates.dismissDialog()
                    AlertTemplates.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
}
