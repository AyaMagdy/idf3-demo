//
//  OfferTableViewCell.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import SDWebImage

class OfferTableViewCell: UITableViewCell {

    
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func SetOffer(offer: Offer)
    {
        logoImage.sd_setImage(with: URL(string: offer.photo), placeholderImage: UIImage(named: "btn 2"))
        nameLabel.text = offer.name
        descriptionLabel.text = offer.description
        discountLabel.text = offer.discount
    }
}
