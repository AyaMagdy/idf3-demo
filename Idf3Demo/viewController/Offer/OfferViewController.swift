//
//  OfferViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class OfferViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var tableView: UITableView!
    var offers = [Offer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomButtons.customBtnObj.backBtn(NC: self.navigationController!, NI: self.navigationItem)
        getOffers()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCellIndex = offers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "offerCell" ) as! OfferTableViewCell
        cell.SetOffer(offer: offerCellIndex)
        return cell
    }
    
    func getOffers()
    {
        AlertTemplates.showDialog(vc: self)
        ApiRequests.ApiObject.offers()
        { result in
            switch result {
            case let .success(offers):
                AlertTemplates.dismissDialog()
                self.offers = offers
                self.tableView.reloadData()
                break
            case let .failure(error):
                AlertTemplates.dismissDialog()
                AlertTemplates.showAlert(vc: self, msg:error)
                break
            }
        }
    }
}
