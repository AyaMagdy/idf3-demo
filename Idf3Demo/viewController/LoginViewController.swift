//
//  LoginViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/4/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var user:User?
    let fcm_token = "123456789"
//    let fcm_token = UserDefaults.standard.string(forKey: Constants.TOKEN)
    let currentVc = self
    @IBOutlet weak var phoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.keyboardType = .asciiCapableNumberPad

    }
    
    @IBAction func loginBtn(_ sender: Any) {
        if phoneTextField.text?.count == 11 {
           loginAndGetUserData()
        }else {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidPhone)
        }
    }
    
    func loginAndGetUserData()
    {
       AlertTemplates.showDialog(vc: self)
        ApiRequests.ApiObject.login(phone: phoneTextField.text!, fcm_token: fcm_token)
        { result in
            switch result {
            case let .success(user):
                print("d5al")
                AlertTemplates.dismissDialog()
                self.user = user
                print("from login controller data is \(user)")
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
                    vc.user = self.user
                    self.present(vc, animated: true, completion: nil)
                }
                break
            case let .failure(error):
                print("not d5al : \(error)")
                AlertTemplates.dismissDialog()
                AlertTemplates.showAlert(vc: self, msg:error)
                break
            }
    }
}
}
