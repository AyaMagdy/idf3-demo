//
//  WalletViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var user_token = ""
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = Cach.getUser(){
            priceLabel.text = user.credit_amount
            user_token = user.user_token
        }
    }
    override func viewDidAppear(_ animated: Bool) {
       getAccountMoney()
    }
    
    @IBAction func reloadBtn(_ sender: Any) {
        getAccountMoney()
    }
    
    func getAccountMoney(){
        ApiRequests.ApiObject.getUserObject(userToken: user_token)
        { result in
            switch result {
            case let .success(user):
                self.user = user
                Cach.saveUser(user: user) // update cach
                self.priceLabel.text = user.credit_amount
                self.currentDate()
                break
            case let .failure(error):
               print(error)
                break
            }
        }
    }

    func currentDate() {
            let date = Date()
            let calender = Calendar.current
            let components = calender.dateComponents([.hour,.minute], from: date)
            
            let hour = components.hour
            let minute = components.minute
            
            if let h = hour , let m = minute {
                lastUpdateLabel.text = "اخر تحديث \(h):\(m) "
            }
        }
}
