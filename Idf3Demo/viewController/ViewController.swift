//
//  ViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/4/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        image.setRounded()
    }

}

