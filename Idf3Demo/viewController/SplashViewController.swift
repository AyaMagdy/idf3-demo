//
//  SplashViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/12/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         checkUser()
    }
    func checkUser() {
        if let user = Cach.getUser(), !user.user_token.isEmpty {
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "old_user", sender: self)
            }
        }else {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "new_user", sender: self)
            }
            
        }
    }
}
