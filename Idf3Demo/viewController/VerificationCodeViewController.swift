//
//  VerificationCodeViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/11/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class VerificationCodeViewController: UIViewController {
    var user:User?

    @IBOutlet weak var verificationCodeTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        verificationCodeTextField.keyboardType = .asciiCapableNumberPad
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is UserDataViewController {
            let vc = segue.destination as? UserDataViewController
            vc?.user = self.user
        }
    }
    @IBAction func nextStepBtn(_ sender: Any) {
        checkVerificationCode()
    }
    func checkVerificationCode(){
          if verificationCodeTextField.text?.count == 4 {
                if (verificationCodeTextField.text == user?.verification_code)
                {
                    performSegue(withIdentifier: "userData", sender: self)
                }
                else
                {
                    AlertTemplates.showAlert(vc:self , msg:Constants.invalidPasscodeLogin)
                }
          }
          else{
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidVerificationCount)
        }
    }
}
