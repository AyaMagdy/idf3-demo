//
//  ChangePinCodeViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/12/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ChangePinCodeViewController: UIViewController {
    var  pincode = 0
    @IBOutlet weak var oldPinCode: UITextField!
    @IBOutlet weak var newPinCode: UITextField!
    @IBOutlet weak var reNewPinCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        oldPinCode.keyboardType = .asciiCapableNumberPad
        newPinCode.keyboardType = .asciiCapableNumberPad
        reNewPinCode.keyboardType = .asciiCapableNumberPad
    }

    @IBAction func saveChange(_ sender: Any) {
        checkPinCode()
    }
    
    func checkPinCode(){
        pincode = Cach.getPasscode()
        if Int(self.oldPinCode.text ?? "0") ?? 0 != pincode {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidOldPasscode)
        }
        else if newPinCode.text?.count != 4 {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidPasscode)
        }else if newPinCode.text != reNewPinCode.text {
            AlertTemplates.showAlert(vc: self, msg: Constants.invalidConfirmPasscode)
        }
        else{
            changePinCode()
        }
    }
    
    func changePinCode(){
        Cach.saveUser(passcode: Int(self.oldPinCode.text ?? "0") ?? 0)
        self.navigationController?.popViewController(animated: true)
        DispatchQueue.global().async {
            
        }
    }
}
