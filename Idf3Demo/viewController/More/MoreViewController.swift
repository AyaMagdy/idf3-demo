//
//  MoreViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    @IBOutlet weak var tableView: UITableView!
    var listName = ["اشحن الان","العروض","عن ادفع","تعديل الحساب الشخصى","تسجيل الخروج"]
    var listImage = [#imageLiteral(resourceName: "recharge"),#imageLiteral(resourceName: "offers-icon"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "edit"),#imageLiteral(resourceName: "logout")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell") as! MoreTableViewCell
        cell.nameLabel.text = listName[indexPath.row]
        cell.iconImage.image = listImage[indexPath.row]
        cell.rightArrow.image = UIImage(named: "back-arrow")
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "recharge", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "offer", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "about", sender: self)
            break
        case 3:
            performSegue(withIdentifier: "edit_profile", sender: self)
            break
        case 4:
            let alert = UIAlertController(title: "تسجيل الخروج", message: "هل انت متاكد من تسجيل الخروج", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
                // remove froom cach
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                dictionary.keys.forEach { key in
                    defaults.removeObject(forKey: key)
                }
                let VC = self.storyboard!.instantiateViewController(withIdentifier: "splashScreen") as! SplashViewController
                self.present(VC, animated:true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "لا", style: UIAlertAction.Style.default, handler: nil))
            
            present(alert, animated: true, completion: nil)
            break

        default:
            break
        }
    }
}
