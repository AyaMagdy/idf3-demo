//
//  HistoryViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
     var history = [Transaction]()
     let user_token = Cach.getUser()?.user_token ?? ""

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         getHistory()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let historyCellIndex = history[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell" ) as! TransactionTableViewCell
        cell.setTransaction(transaction: historyCellIndex)
        return cell
    }
    func getHistory()
    {
        AlertTemplates.showDialog(vc: self)
        ApiRequests.ApiObject.history(user_token : user_token)
        { result in
            switch result {
            case let .success(transactions):
                AlertTemplates.dismissDialog()
                self.history = transactions
                self.tableview.reloadData()
                break
            case let .failure(error):
                AlertTemplates.dismissDialog()
                AlertTemplates.showAlert(vc: self, msg:error)
                break
            }
        }
    }
}
