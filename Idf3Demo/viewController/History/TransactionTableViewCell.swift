//
//  TransactionTableViewCell.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTransaction(transaction : Transaction)
    {
        
        nameLabel.text = "الإسم : \(transaction.name)"
        priceLabel.text =  "\(transaction.credit_amount) جنية"
        
        dateLabel.text = "التاريخ : \( DateFormat.convertDate(date: transaction.created_at) )"
        
        if transaction.is_sender {
            phoneLabel.text = "إرسال الى :  \(transaction.phone)"
            priceLabel.textColor = UIColor.red
            iconImage.image = #imageLiteral(resourceName: "Send")
        }else {
            phoneLabel.text = "إستقبال من :  \(transaction.phone)"
            priceLabel.textColor = UIColor.green
            iconImage.image = #imageLiteral(resourceName: "receive")
        }
    }

}
