//
//  ScanQRCodeViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import AVFoundation

class ScanQRCodeViewController: UIViewController ,AVCaptureMetadataOutputObjectsDelegate {
    
    
    var video = AVCaptureVideoPreviewLayer()
    
    let output = AVCaptureMetadataOutput()
    var transferProtocol : TransferProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        transferProtocol?.completeTransaction(phone: "01273144337")
        scanQrCode()
    }
    
    func scanQrCode() {
        
        // create session
        let session = AVCaptureSession()
        // define caputre device
        let caputreDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: caputreDevice!)
            session.addInput(input)
        }catch {
            
        }
        
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        video = AVCaptureVideoPreviewLayer(session: session)
        // fill the entire screen
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        session.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr {
                    output.setMetadataObjectsDelegate(nil, queue: DispatchQueue.main)
                    self.navigationController?.popViewController(animated: true)
                    self.transferProtocol?.completeTransaction(phone: object.stringValue ?? "")
                }
            }
        }
    }
    
}

