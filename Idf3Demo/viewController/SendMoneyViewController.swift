//
//  SendMoneyViewController.swift
//  Idf3Demo
//
//  Created by aya magdy on 3/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SendMoneyViewController: UIViewController , TransferProtocol {

    

    @IBOutlet weak var moneyAmount: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func SecondStepBtn(_ sender: Any) {
        if let money = moneyAmount.text , !money.isEmpty
        {
             showTransactionOption()
        }
        else
        {
            let alert = UIAlertController(title: "خطا", message: "برجاء ادخال المبلغ",preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ScanQRCodeViewController {
            let vc = segue.destination as? ScanQRCodeViewController
            vc?.transferProtocol = self
        }else if segue.destination is TransferUsingPhoneViewController {
            let vc = segue.destination as? TransferUsingPhoneViewController
            vc?.transferProtocol = self
        }
    }
    func showTransactionOption() {
        let alertController = UIAlertController(title: nil, message: "اختر طريقة الارسال", preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "فحص رمز QR", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.performSegue(withIdentifier: "scanQRCode", sender: self)
        })
        
        let action2 = UIAlertAction(title: "رقم الهاتف", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.performSegue(withIdentifier: "enterPhoneNumber", sender: self)
        })
        
        
        let maybeAction = UIAlertAction(title: "الغاء", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Maybe")
        })
        
        let image1 = UIImage(named: "qr-code")
        let image2 = UIImage(named: "telephone-keypad-with-ten-keys")
        
        
        action1.setValue(image1, forKey: "image")
        action1.setValue(AlertTemplates.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
       action2.setValue(image2, forKey: "image")
        action2.setValue(AlertTemplates.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
        
        maybeAction.setValue(UIColor.red, forKey: "titleTextColor")
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(maybeAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func completeTransaction(phone: String) {
        if let money = moneyAmount.text {
            let refreshAlert = UIAlertController(title: "", message: "هل انت موافق على ارسال مبلغ \(money) الى \(phone)", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { (action: UIAlertAction!) in
                self.transaction(number: phone)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "لا", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func transaction(number:String) {
        print (number)
        if let user = Cach.getUser() {
            AlertTemplates.showDialog(vc: self)
            ApiRequests.ApiObject.transferMoney(userToken: user.user_token, toPhone: number, amount: moneyAmount.text ?? "0")  { result in
                print(result)
                switch result {
                case .success(_):
                    AlertTemplates.dismissDialog()
                    AlertTemplates.showAlert(vc: self, title:"نجحت العملية",msg: "تم ارسال المبلغ بنجاح")
                    self.transactionDone()
                    self.moneyAmount.text = ""
                    break
                case let .failure(error):
                    AlertTemplates.dismissDialog()
                    AlertTemplates.showAlert(vc: self, msg:error)
                    break
                }
            }
        }else {
            AlertTemplates.showAlert(vc: self, title:"فشلت العملية",msg: "حدث خطا ما")
        }
    }
    
    
    func transactionDone()  {
        if var user = Cach.getUser(),let money = moneyAmount.text {
            let result = Int(user.credit_amount)! - Int(money)!
            user.credit_amount = String(result)
            Cach.saveUser(user: user)
        }
    }
}
